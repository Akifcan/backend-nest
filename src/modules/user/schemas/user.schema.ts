import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { ObjectType, Field, ID } from '@nestjs/graphql';
import { USER_ROLES } from '../../auth/dtos/register.dto';
import * as md5 from 'md5';

export type UserDocument = User & Document;
@ObjectType()
@Schema({ timestamps: true })
export class User {
  @Prop({ required: true, maxlength: 50 })
  @Field()
  username: string;

  @Field()
  @Prop({ required: true, minlength: 5 })
  password: string;

  @Field()
  @Prop({ required: true, unique: true })
  email: string;

  @Field()
  @Prop({ required: true, enum: ['employee', 'manager'] })
  role: USER_ROLES;
}

@ObjectType()
export class UserType extends User {
  @Field(() => ID)
  _id: Types.ObjectId;
  @Field(() => Date)
  createdAt: Date;
  @Field()
  token: string;
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.pre('save', function (next) {
  const user = this as unknown as User;
  user.password = md5(user.password);
  next();
});
