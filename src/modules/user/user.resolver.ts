import { Resolver } from '@nestjs/graphql';
import { Query, Args, Int } from '@nestjs/graphql';

@Resolver()
export class UserResolver {
  @Query(() => Number)
  hello(@Args('id', { type: () => Int }) id: number) {
    return id;
  }
}
