import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import LoginDto from '../auth/dtos/login.dto';
import RegisterDto from '../auth/dtos/register.dto';
import { User, UserDocument } from './schemas/user.schema';
import * as md5 from 'md5';

@Injectable()
export class UserService {
  @InjectModel(User.name) private userModel: Model<UserDocument>;

  async register(user: RegisterDto) {
    return await this.userModel.create(user);
  }

  async login(user: LoginDto) {
    const result = await this.userModel
      .findOne({ email: user.email, password: md5(user.password) })
      .lean();
    if (result != null) {
      return result;
    } else {
      throw new UnauthorizedException();
    }
  }
}
