import {
  Injectable,
  CanActivate,
  ExecutionContext,
  Inject,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';
import { UserType } from '../user/schemas/user.schema';

export class CurrentUser {
  user: UserType;
}

@Injectable()
class AuthGuard implements CanActivate {
  @Inject() private jwtService: JwtService;

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const gqlCtx = GqlExecutionContext.create(context);
    const request = gqlCtx.getContext().req;
    if (request.headers.token) {
      gqlCtx.getContext().user = this.jwtService.decode(request.headers.token);
      return true;
    } else {
      return false;
    }
  }
}

export default AuthGuard;
