import {
  Injectable,
  CanActivate,
  ExecutionContext,
  Inject,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';
import { UserType } from '../user/schemas/user.schema';
import { USER_ROLES } from './dtos/register.dto';

@Injectable()
class ManagerGuard implements CanActivate {
  @Inject() private jwtService: JwtService;

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const gqlCtx = GqlExecutionContext.create(context);
    const user = gqlCtx.getContext().user as UserType;
    return user.role === USER_ROLES.manager;
  }
}

export default ManagerGuard;
