import { Inject } from '@nestjs/common';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { UserType } from '../user/schemas/user.schema';
import { UserService } from '../user/user.service';
import LoginDto from './dtos/login.dto';
import RegisterDto from './dtos/register.dto';
import { JwtService } from '@nestjs/jwt';

@Resolver()
export class AuthResolver {
  @Inject() private userService: UserService;
  @Inject() private jwtService: JwtService;

  @Mutation(() => UserType)
  async register(@Args('register') user: RegisterDto) {
    const result = await this.userService.register(user);
    return { ...result['_doc'], token: this.jwtService.sign(result['_doc']) };
  }

  @Mutation(() => UserType)
  async login(@Args('login') user: LoginDto) {
    const result = await this.userService.login(user);
    return { ...result, token: this.jwtService.sign(result) };
  }
}
