import { InputType, Field, registerEnumType } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty, MinLength } from 'class-validator';

export enum USER_ROLES {
  employee = 'employee',
  manager = 'manager',
}

registerEnumType(USER_ROLES, {
  name: 'UserRoles',
});

@InputType()
class RegisterDto {
  @IsNotEmpty()
  @Field()
  username: string;

  @IsNotEmpty()
  @MinLength(5)
  @Field()
  password: string;

  @IsNotEmpty()
  @IsEmail()
  @Field()
  email: string;

  @IsNotEmpty()
  @Field(() => USER_ROLES)
  role: USER_ROLES;
}

export default RegisterDto;
