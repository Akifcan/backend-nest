import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { ObjectType, Field, ID } from '@nestjs/graphql';
import { UserType } from '../../user/schemas/user.schema';

export enum JobStatus {
  PROGRESS = 'progress',
  DONE = 'done',
}

export type JobDocument = Job & Document;
@ObjectType()
@Schema({ timestamps: true })
export class Job {
  @Prop({ required: true })
  @Field()
  title: string;

  @Prop({ required: true })
  @Field()
  description: string;

  @Prop({ required: true })
  @Field()
  lat: number;

  @Prop({ required: true })
  @Field()
  long: number;

  @Prop({ required: true })
  @Field(() => [String])
  employees: string[];

  @Prop({ required: true, type: [String] })
  @Field(() => [String])
  images: string[];

  @Field(() => String)
  @Prop({ required: true, type: Types.ObjectId })
  assignedId: Types.ObjectId;

  @Field()
  @Prop({ default: 'progress', type: String, enum: ['done', 'progress'] })
  status: JobStatus;
}
@ObjectType()
export class JobType extends Job {
  @Field(() => ID)
  _id: Types.ObjectId;
  @Field(() => Date)
  createdAt: Date;
}
@ObjectType()
export class JobTypeAssignedBy extends JobType {
  @Field(() => UserType)
  assignedBy: UserType;
}

export const JobSchema = SchemaFactory.createForClass(Job);
