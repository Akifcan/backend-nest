import { IsNotEmpty, IsNumber } from 'class-validator';
import { InputType, Field } from '@nestjs/graphql';

@InputType()
class CreateJobDto {
  @Field()
  @IsNotEmpty()
  title: string;

  @Field()
  @IsNotEmpty()
  description: string;

  @Field()
  @IsNotEmpty()
  @IsNumber()
  lat: number;

  @Field()
  @IsNotEmpty()
  @IsNumber()
  long: number;

  @Field(() => [String])
  @IsNotEmpty()
  employees: string[];

  @Field(() => [String])
  @IsNotEmpty()
  images: string[];
}

export default CreateJobDto;
