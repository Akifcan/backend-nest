import { InputType, Field, registerEnumType } from '@nestjs/graphql';
import { JobStatus } from '../schemas/job.schema';
import { IsNotEmpty } from 'class-validator';

registerEnumType(JobStatus, {
  name: 'JobStatus',
});

@InputType()
class UpdateJobStatusDto {
  @Field()
  @IsNotEmpty()
  id: string;
  @Field(() => JobStatus)
  status: JobStatus;
}

export default UpdateJobStatusDto;
