import { Inject, UseGuards } from '@nestjs/common';
import { Query, Resolver, Mutation, Args, Context } from '@nestjs/graphql';
import AuthGuard, { CurrentUser } from '../auth/auth.guard';
import ManagerGuard from '../auth/manager.guard';
import CreateJobDto from './dtos/createJob.dto';
import UpdateJobStatusDto from './dtos/updateJobStatus.dto';
import { JobsService } from './jobs.service';
import { JobStatus, JobType, JobTypeAssignedBy } from './schemas/job.schema';

@Resolver()
@UseGuards(AuthGuard)
export class JobsResolver {
  @Inject() private jobService: JobsService;

  @UseGuards(ManagerGuard)
  @Query(() => [JobTypeAssignedBy])
  async listJobs() {
    return await this.jobService.listJobs();
  }

  @UseGuards(ManagerGuard)
  @Query(() => [JobTypeAssignedBy])
  async filterJobs(
    @Args('status', { type: () => JobStatus }) status: JobStatus,
  ) {
    return await this.jobService.filterJobs(status);
  }

  @Query(() => [JobTypeAssignedBy])
  async listMyJobs(@Context() context: CurrentUser) {
    return await this.jobService.listMyJobs(context.user._id);
  }

  @UseGuards(ManagerGuard)
  @Mutation(() => Boolean)
  async updateJobStatus(@Args('job') job: UpdateJobStatusDto) {
    return await this.jobService.setStatus(job.id, job.status);
  }

  @Mutation(() => JobType)
  async assignJob(
    @Args('job') job: CreateJobDto,
    @Context() context: CurrentUser,
  ) {
    return await this.jobService.assignJob(job, context.user._id);
  }
}
