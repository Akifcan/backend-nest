import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import CreateJobDto from './dtos/createJob.dto';
import { Job, JobDocument, JobStatus } from './schemas/job.schema';

const lookupQuery = [
  {
    $sort: { createdAt: -1 },
  },
  {
    $lookup: {
      from: 'users',
      localField: 'assignedId',
      foreignField: '_id',
      as: 'assignedBy',
    },
  },
  {
    $unwind: '$assignedBy',
  },
];

@Injectable()
export class JobsService {
  @InjectModel(Job.name) private jobModel: Model<JobDocument>;

  async assignJob(job: CreateJobDto, assignedBy: Types.ObjectId) {
    const { employees, ...item } = job;
    const employeeIds = employees.map(
      (employee) => new Types.ObjectId(employee),
    );
    return await this.jobModel.create({
      ...item,
      employees: employeeIds,
      assignedId: new Types.ObjectId(assignedBy),
    });
  }

  async setStatus(jobId: string, status: JobStatus) {
    const result = await this.jobModel.updateOne(
      { _id: new Types.ObjectId(jobId) },
      {
        status,
      },
    );
    if (result.modifiedCount > 0) {
      return true;
    } else {
      return false;
    }
  }

  async listJobs() {
    return await this.jobModel.aggregate([...lookupQuery]);
  }

  async listMyJobs(userId: Types.ObjectId) {
    return await this.jobModel.aggregate([
      {
        $match: { employees: { $in: [new Types.ObjectId(userId)] } },
      },
      ...lookupQuery,
    ]);
  }

  async filterJobs(status: JobStatus) {
    return await this.jobModel.aggregate([
      {
        $match: { status },
      },
      ...lookupQuery,
    ]);
  }
}
