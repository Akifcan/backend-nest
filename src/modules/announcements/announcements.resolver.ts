import { Inject, UseGuards } from '@nestjs/common';
import { Query, Resolver, Context, Mutation, Args } from '@nestjs/graphql';
import AuthGuard, { CurrentUser } from '../auth/auth.guard';
import ManagerGuard from '../auth/manager.guard';
import { AnnouncementsService } from './announcements.service';
import CreateAnnouncementDto from './dtos/create.announcement';

import {
  AnnouncementCreatedBy,
  AnnouncementType,
} from './schemas/announcement.schema';

@Resolver()
@UseGuards(AuthGuard)
export class AnnouncementsResolver {
  @Inject() private announcementService: AnnouncementsService;

  @Query(() => [AnnouncementCreatedBy])
  async listAnnouncements() {
    return await this.announcementService.list();
  }

  @UseGuards(ManagerGuard)
  @Mutation(() => AnnouncementType)
  async createAnnouncements(
    @Args('announcement') announcement: CreateAnnouncementDto,
    @Context() user: CurrentUser,
  ) {
    return await this.announcementService.create(announcement, user.user._id);
  }
}
