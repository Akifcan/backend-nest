import { InputType, Field } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
class CreateAnnouncementDto {
  @IsNotEmpty()
  @Field()
  title: string;
  @Field()
  @IsNotEmpty()
  description: string;
}

export default CreateAnnouncementDto;
