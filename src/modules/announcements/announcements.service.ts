import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import CreateAnnouncementDto from './dtos/create.announcement';
import {
  Announcement,
  AnnouncementDocument,
} from './schemas/announcement.schema';

@Injectable()
export class AnnouncementsService {
  @InjectModel(Announcement.name)
  private announcementModel: Model<AnnouncementDocument>;

  async create(announcement: CreateAnnouncementDto, createdBy: Types.ObjectId) {
    return await this.announcementModel.create({
      ...announcement,
      createdBy: new Types.ObjectId(createdBy),
    });
  }

  async list() {
    return await this.announcementModel.aggregate([
      {
        $sort: { createdAt: -1 },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'createdBy',
          foreignField: '_id',
          as: 'shared',
        },
      },
      {
        $unwind: '$shared',
      },
    ]);
  }
}
