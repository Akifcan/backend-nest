import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { ObjectType, Field, ID } from '@nestjs/graphql';
import { UserType } from '../../user/schemas/user.schema';

export type AnnouncementDocument = Announcement & Document;
@ObjectType()
@Schema({ timestamps: true })
export class Announcement {
  @Field()
  @Prop({ required: true })
  title: string;
  @Prop({ required: true })
  @Field()
  description: string;
  @Prop({ required: true, type: Types.ObjectId })
  createdBy: Types.ObjectId;
}
@ObjectType()
export class AnnouncementType extends Announcement {
  @Field(() => ID)
  _id: Types.ObjectId;
  @Field(() => Date)
  createdAt: Date;
}
@ObjectType()
export class AnnouncementCreatedBy extends AnnouncementType {
  @Field(() => UserType)
  shared: UserType;
}

export const AnnouncementSchema = SchemaFactory.createForClass(Announcement);
