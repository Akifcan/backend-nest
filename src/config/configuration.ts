export default () => ({
  database: {
    host: process.env.DB_HOST,
  },
  jwt: {
    secret: process.env.JWT_SECRET,
  },
});
