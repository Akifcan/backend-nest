import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AppModule } from './../src/app.module';
import { createTestClient, TestQuery } from 'apollo-server-integration-testing';
import { GraphQLModule } from '@nestjs/graphql';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let mutateTest: TestQuery;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    const module: GraphQLModule =
      moduleFixture.get<GraphQLModule>(GraphQLModule);

    const { mutate } = createTestClient({
      apolloServer: (module as any).apolloServer,
    });

    mutateTest = mutate;
  });

  it('Login when informations are correct', async () => {
    const result = await mutateTest(`
      mutation LOGIN {
            login(login: {email: "lena@mail.com", password:"123456"}){
                username,
                email,
                role,
                token
            }
         }
    `);
    expect(result.data).toBeDefined();
  });

  it('Login when informations are incorrect', async () => {
    const result = await mutateTest(`
      mutation LOGIN {
            login(login: {email: "lena@mail.com", password:"123443356"}){
                username,
                email,
                role,
                token
            }
         }
    `);
    expect(result.errors).toBeDefined();
  });
});
