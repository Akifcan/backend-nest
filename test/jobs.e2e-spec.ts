import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AppModule } from './../src/app.module';
import { createTestClient, TestQuery } from 'apollo-server-integration-testing';
import { GraphQLModule } from '@nestjs/graphql';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let correctQueryTest: TestQuery;
  let wrongQueryTest: TestQuery;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    const module: GraphQLModule =
      moduleFixture.get<GraphQLModule>(GraphQLModule);

    const { query: correctQuery } = createTestClient({
      apolloServer: (module as any).apolloServer,
      extendMockRequest: {
        headers: {
          token:
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MWFiNmY0MjQ3YjEyYWNiNzQyYmQwYmYiLCJyb2xlIjoibWFuYWdlciIsImVtYWlsIjoibGVuYUBtYWlsLmNvbSIsInBhc3N3b3JkIjoiZTEwYWRjMzk0OWJhNTlhYmJlNTZlMDU3ZjIwZjg4M2UiLCJ1c2VybmFtZSI6ImxlbmEgZG9lIiwiY3JlYXRlZEF0IjoiMjAyMS0xMi0wNFQxMzozODoxMC4xMzZaIiwidXBkYXRlZEF0IjoiMjAyMS0xMi0wNFQxMzozODoxMC4xMzZaIiwiX192IjowLCJpYXQiOjE2Mzg2OTIyNzEsImV4cCI6MTYzODY5MjMzMX0.RamD2asuWaaAGHzvlAKqzRpBXLbojVkqVhVDNF21RkE',
        },
      },
    });

    const { query: wrongQuery } = createTestClient({
      apolloServer: (module as any).apolloServer,
    });

    correctQueryTest = correctQuery;
    wrongQueryTest = wrongQuery;
  });

  it('Should data available when token exists', async () => {
    const result = await correctQueryTest(`
        query FILTER_JOBS{
            filterJobs(status: DONE) {
                    title,
                    status,
                    description,
                    lat,
                    long,
                    employees,
                    images,
                    assignedBy {
                    username,
                    email
                    }
                }
            }
      `);
    expect(result.data).toBeDefined();
  });

  it('Should throw error when token does not exists', async () => {
    const result = await wrongQueryTest(`
        query FILTER_JOBS{
            filterJobs(status: DONE) {
                    title,
                    status,
                    description,
                    lat,
                    long,
                    employees,
                    images,
                    assignedBy {
                    username,
                    email
                    }
                }
            }
      `);
    expect(result.errors).toBeDefined();
  });
});
